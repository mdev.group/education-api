import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Course {
  @Prop({required: true})
  name: string;

  @Prop()
  teacherID: string;

  @Prop({required: true})
  create_datetime: number;

  @Prop()
  minAge: number;

  @Prop()
  maxAge: number;

  @Prop()
  description: string;

  @Prop()
  photo_url: string;

  @Prop()
  show: boolean;
}

export type CourseDocument = Course & Document;
export const CourseSchema = SchemaFactory.createForClass(Course);
