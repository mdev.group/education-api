import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Notification, NotificationSchema } from 'src/schemas/notification.schema';
import { CourseModule } from '../course/course.module';
import { GroupModule } from '../group/group.module';
import { PupilModule } from '../pupil/pupil.module';
import { UserModule } from '../user/user.module';
import { NotificationController } from './notification.controller';
import { NotificationService } from './notification.service';

@Module({
  controllers: [NotificationController],
  providers: [NotificationService],
  imports: [
    MongooseModule.forFeature([{ name: Notification.name, schema: NotificationSchema }]),
    forwardRef(() => PupilModule),
    forwardRef(() => UserModule)
  ],
  exports: [
    MongooseModule.forFeature([{ name: Notification.name, schema: NotificationSchema }]),
    NotificationService,
  ],
})
export class NotificationModule {}
