import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, forwardRef, Inject } from '@nestjs/common';
import { Task, TaskDocument } from 'src/schemas/task.schema';
import { InjectModel } from '@nestjs/mongoose';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { CourseDocument } from 'src/schemas/course.schema';
import { LevelService } from '../level/level.service';
import { PupilService } from '../pupil/pupil.service';

@Injectable()
export class TaskService {
  constructor(
    @InjectModel(Task.name) private TaskModel: Model<TaskDocument>,
    @Inject(forwardRef(() => LevelService))
    private levelService: LevelService,
    @Inject(forwardRef(() => PupilService))
    private pupilService: PupilService,
  ) {}

  async getLevel(levelID: string, user) {
    const course = await this.levelService.findByID(levelID, user);
    return course;
  }

  async getCourse(levelID: string, user) {
    const level = await this.getLevel(levelID, user);
    const course = await this.levelService.getCourse(level, user);
    return course;
  }

  async userIsOwner(levelID: string, user) {
    const level = await this.getLevel(levelID, user);
    return await this.levelService.userIsOwner(level, user);
  }

  async userIsStudent(levelID: string, user) {
    const level = await this.getLevel(levelID, user);
    return await this.levelService.userIsStudent(level, user);
  }

  async create(data, user): Promise<Task> {
    const isCourseOwner = await this.userIsOwner(data.levelID, user);
    if(!isCourseOwner) {
      throw new NotAcceptableException('Course');
    }

    const created = new this.TaskModel({
      levelID: data.levelID,
      text: data.text,
      hint: data.hint,
      name: data.name,
      field: [],
      startPos: '0;0',
      finalPos: '0;0',
      puzzles: []
    });

    if(created) {
      return created.save();
    } else {
      throw new ForbiddenException();
    }
  }

  async solve(taskID, user): Promise<any> {
    const task = await this.TaskModel.findById(taskID);
    
    if(!task) {
      throw new NotFoundException("Task")
    }

    const level = await this.getLevel(task.levelID, user);

    this.pupilService.setCourseProgress(user.userId, level.courseID, taskID, user)

    return {
      res: true
    }
  }

  async findByID(ID, user): Promise<Task> {
    const task = await this.TaskModel.findById(ID);
    
    if(!task) {
      throw new NotFoundException("Task")
    }
    if(!this.userIsOwner(task.levelID, user) && !this.userIsStudent(task.levelID, user)) {
      throw new NotAcceptableException("Task - Course")
    }

    return task;
  }
  
  async findByLevelID(levelID, user): Promise<Task[]> {
    await this.levelService.findByID(levelID, user);

    // const isOwnerOrStudent = (await this.userIsOwner(levelID, user)) || (await this.userIsStudent(levelID, user));
    // if (!isOwnerOrStudent) {
    //   throw new NotAcceptableException("Task - Course")
    // }

    return await this.TaskModel.find({
      levelID
    }).exec();
  }

  async update(ID, data, user): Promise<Task> {
    const task = await this.findByID(ID, user);

    return this.TaskModel
      .findByIdAndUpdate(ID, {
        $set: {
          ...removeEmptyFields(data),
        },
      },
      {new: true})
      .exec();
  }

  async delete(ID, user): Promise<any> {
    const task = await this.findByID(ID, user);

    if(!this.userIsOwner(task.levelID, user)) {
      throw new NotAcceptableException("Course");
    }

    await this.TaskModel.findOneAndDelete(ID);

    return {
      res: true
    };
  }
}
