import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, forwardRef, Inject } from '@nestjs/common';
import { Group, GroupDocument } from 'src/schemas/group.schema';
import { InjectModel } from '@nestjs/mongoose';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { genSaltSync, hashSync } from 'bcrypt';
import { PupilService } from '../pupil/pupil.service';

@Injectable()
export class GroupService {
  constructor(
    @InjectModel(Group.name) private GroupModel: Model<GroupDocument>,
    @Inject(forwardRef(() => PupilService))
    private pupilService: PupilService,
  ) {}

  userIsOwner(group: Group, user) {
    return group.teacherID === user.userId;
  }

  async create(data, user): Promise<Group> {
    const created = new this.GroupModel({
      name: data.name,
      teacherID: user.userId,
      courseID: data.courseID
    });

    if(created) {
      return await created.save();
    } else {
      throw new ForbiddenException();
    }
  }


  async appendStudents(group: any, user): Promise<any> {
    group.students = await this.pupilService.findByGroupID(group._id, user);
    return group;
  }


  async appendEntries(group: GroupDocument, user): Promise<any> {
    group = await this.appendStudents(group, user);
    return group;
  }

  async findByTeacherID(teacherID, user): Promise<Group[]> {
    if(teacherID != user.userId) {
      throw new NotAcceptableException("Teacher")
    }

    const groups = await this.GroupModel.find({
      teacherID
    }).exec()

    return await Promise.all(groups.map((group) => this.appendEntries(group.toObject() as any, user)));
  }

  async findByID(ID, user, append = false): Promise<Group> {
    const group = await this.GroupModel.findById(ID);

    if(!group) {
      throw new NotFoundException("Group")
    }
    if(!this.userIsOwner(group, user) && !this.pupilService.userIsStudentOfGroup(group._id, user)) {
      throw new NotAcceptableException("Group")
    }

    if(append) {
      return await this.appendEntries(group.toObject() as any, user);
    } else {
      return group.toObject()
    }
  }

  async update(ID, data, user): Promise<Group> {
    const group = await this.GroupModel.findById(ID);
    if(!group) {
      throw new NotFoundException("Group")
    }
    if(!this.userIsOwner(group, user)) {
      throw new NotAcceptableException("Group")
    }

    return this.GroupModel
      .findByIdAndUpdate(ID, {
        $set: {
          ...removeEmptyFields(data),
        },
      },
      {new: true})
      .exec();
  }

  async delete(ID, user): Promise<any> {
    const group = await this.GroupModel.findById(ID);

    if(!group) {
      throw new NotFoundException("Group")
    }
    if(!this.userIsOwner(group, user)) {
      throw new NotAcceptableException("Group")
    }

    if((await this.pupilService.findByGroupID(ID, user, false)).length > 0) {
      throw new ForbiddenException("Pupils in group")
    }

    await group.deleteOne();

    return {
      res: true
    };
  }
}
