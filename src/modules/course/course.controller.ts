import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Course } from 'src/schemas/course.schema';
import { CourseService } from './course.service';
import { Role } from '../auth/role.enum';
import RoleGuard from '../auth/role.guard';
import { PupilService } from '../pupil/pupil.service';
import { Pupil } from 'src/schemas/pupil.schema';

@Controller()
export class CourseController {
  constructor(
    private readonly courseService: CourseService,
    private readonly pupilService: PupilService
  ) {}

  @UseGuards(RoleGuard([Role.Teacher, Role.Pupil]))
  @Get('/course')
  async getAll(@Request() req): Promise<Course[]> {
    return await this.courseService.findAll(req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Get('/course/owned')
  async getOwned(@Request() req): Promise<Course[]> {
    return await this.courseService.findByCreatorID(req.user.userId, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Post('/course/search')
  async search(@Request() req, @Body() data): Promise<Course[]> {
    return await this.courseService.searchByName(data.searchString, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Get('/course/:id')
  get(@Request() req, @Param() params) {
    return this.courseService.findByID(params.id, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher, Role.Pupil]))
  @Get('/course/:id/structure')
  getStruct(@Request() req, @Param() params) {
    return this.courseService.getStructureByID(params.id, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Post('/course')
  create(@Request() req, @Body() data) {
    return this.courseService.create(data, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Patch('/course/:id')
  update(@Request() req, @Body() data, @Param() params) {
    return this.courseService.update(params.id, data, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Get('/course/:id/pupil')
  async getCoursePupils(@Request() req, @Param() params): Promise<Pupil[]> {
    return await this.pupilService.findByCourseID(params.id, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Delete('/course/:id')
  async delete(@Request() req, @Param() params): Promise<any> {
    return await this.courseService.delete(params.id, req.user);
  }

}
