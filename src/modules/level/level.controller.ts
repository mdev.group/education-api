import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Level } from 'src/schemas/level.schema';
import { LevelService } from './level.service';
import { Role } from '../auth/role.enum';
import RoleGuard from '../auth/role.guard';
import { PupilService } from '../pupil/pupil.service';
import { Pupil } from 'src/schemas/pupil.schema';

@Controller()
export class LevelController {
  constructor(
    private readonly levelService: LevelService
  ) {}

  @UseGuards(RoleGuard([Role.Teacher,]))
  @Get('/level/owned')
  async getOwned(@Request() req): Promise<Level[]> {
    return await this.levelService.findByCreatorID(req.user.userId, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Get('/level/:id')
  get(@Request() req, @Param() params) {
    return this.levelService.findByID(params.id, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Get('/level/teacher/:id')
  getByTeacher(@Request() req, @Param() params) {
    return this.levelService.findByCreatorID(params.id, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Get('/level/course/:id')
  getByCourseID(@Request() req, @Param() params) {
    return this.levelService.findByCourseID(params.id, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Post('/level')
  create(@Request() req, @Body() data) {
    return this.levelService.create(data, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Patch('/level/:id')
  update(@Request() req, @Body() data, @Param() params) {
    return this.levelService.update(params.id, data, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Delete('/level/:id')
  async delete(@Request() req, @Param() params): Promise<any> {
    return await this.levelService.delete(params.id, req.user);
  }
}
