export enum Role {
  Teacher = 'teacher',
  Pupil = 'pupil',
  Admin = 'admin',
}