type TLoginTypeArr = [
  auth_date: number,
  first_name: string,
  hash: string,
  id: number,
  last_name: string,
  photo_url: string,
  username: string
]

interface ILoginType {
  auth_date: number;
  first_name: string;
  hash: string;
  id: number;
  last_name: string;
  photo_url: string;
  username: string;
}