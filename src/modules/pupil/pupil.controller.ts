import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Pupil } from 'src/schemas/pupil.schema';
import { PupilService } from './pupil.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import RoleGuard from '../auth/role.guard';
import { Role } from '../auth/role.enum';

@Controller()
export class PupilController {
  constructor(private readonly pupilService: PupilService) {}

  @UseGuards(RoleGuard([Role.Teacher]))
  @Post('/pupil')
  async create(@Request() req, @Body() data): Promise<Pupil> {
    return await this.pupilService.create(data, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Post('/pupil/search')
  async search(@Request() req, @Body() data): Promise<Pupil[]> {
    return await this.pupilService.searchByUsername(data.searchString, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Get('/pupil')
  async getMyPupils(@Request() req): Promise<Pupil[]> {
    return await this.pupilService.findByCreatorID(req.user.userId);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Get('/pupil/group/:id')
  async getPupilsByGroup(@Request() req, @Param() params): Promise<Pupil[]> {
    return await this.pupilService.findByGroupID(params.id, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Get('/pupil/:id')
  async getPupil(@Request() req, @Param() params): Promise<Pupil> {
    let pupil: Pupil;

    if(params.id === 'self') {
      pupil = await this.pupilService.findByID(req.user.userId, req.user);
    } else {
      pupil = await this.pupilService.findByID(params.id, req.user);
    }

    if(!pupil) {
      throw new NotFoundException();
    }

    return pupil;
  }

  @UseGuards(RoleGuard([Role.Teacher, Role.Pupil]))
  @Patch('/pupil/:id')
  async update(@Param() params, @Request() req, @Body() data) {
    let updID = params.id;
    if(params.id === 'me') {
      updID = req.user.userId
    }
    return await this.pupilService.update(updID, data, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Patch('/pupil/:id/group')
  async assignPupilToGroup(@Param() params, @Body() data, @Request() req): Promise<Pupil> {
    return await this.pupilService.assignToGroup(params.id, data.groupID, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher, Role.Pupil]))
  @Patch('/pupil/:id/course')
  async assignPupilToCourse(@Param() params, @Body() data, @Request() req): Promise<Pupil> {
    return await this.pupilService.assignToCourse(params.id, data.courseID, data.value, req.user);
  }

  @UseGuards(RoleGuard([Role.Pupil]))
  @Patch('/pupil/:id/teacher')
  async assignPupilToTeacher(@Param() params, @Body() data, @Request() req): Promise<Pupil> {
    let updID = params.id;
    if(params.id === 'me') {
      updID = req.user.userId
    }
    return await this.pupilService.assignToTeacher(updID, data.teacherID, req.user);
  }


  @UseGuards(RoleGuard([Role.Pupil]))
  @Get('/pupil/course/my')
  async getAssignedCourses(@Request() req): Promise<Pupil> {
    return await this.pupilService.getAssignedCourses(req.user.userId, req.user);
  }
}
