import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, forwardRef, Inject } from '@nestjs/common';
import { EUserType, Notification, NotificationDocument, NotifyItem } from 'src/schemas/notification.schema';
import { InjectModel } from '@nestjs/mongoose';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { genSaltSync, hashSync } from 'bcrypt';
import { letFields } from 'src/utils/letFields';
import { UserService } from '../user/user.service';
import { PupilService } from '../pupil/pupil.service';
import { User } from 'src/schemas/user.schema';
import { Pupil } from 'src/schemas/pupil.schema';

@Injectable()
export class NotificationService {
  constructor(
    @InjectModel(Notification.name) private NotificationModel: Model<NotificationDocument>,
    @Inject(forwardRef(() => PupilService))
    private pupilService: PupilService,
    @Inject(forwardRef(() => UserService))
    private userService: UserService,
  ) {}

  async create (userID: string, userType: EUserType, data: NotifyItem[], user: any): Promise<Notification> {
    let account: User | Pupil;
    if(userType === EUserType.Student) {
      account = await this.pupilService.findByID(userID, null)
    } else {
      account = await this.userService.findByID(userID)
    }

    const created = new this.NotificationModel({
      create_datetime: Number(new Date()),
      data: data,
      userType: userType,
      userID: userID,
      createdBy: user?.userId
    });

    if(created) {
      return created.save();
    } else {
      throw new ForbiddenException();
    }
  }

  async findByUserID (userID: string, user: any): Promise<Notification[]> {
    if(userID !== user.userId) {
      throw new NotAcceptableException('Notification');
    }

    const notifies = await this.NotificationModel.find({
      userID: userID
    }).sort({
      create_datetime: -1
    });

    return notifies;
  }
}
