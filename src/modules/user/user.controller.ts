import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  Request,
  UseGuards,
} from '@nestjs/common';
import { User } from 'src/schemas/user.schema';
import { Pupil } from 'src/schemas/pupil.schema';
import { UserService } from './user.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UpdateUserDto } from './dto/update-user.dto';
import { Role } from '../auth/role.enum';
import RoleGuard from '../auth/role.guard';
import { PupilService } from '../pupil/pupil.service';

@Controller()
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly pupilService: PupilService
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get('/user')
  async getAllUsers(): Promise<User[]> {
    return await this.userService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get('/user/me')
  async getMe(@Request() req): Promise<User | Pupil> {
    if(req.user.roles.includes(Role.Pupil)) {
      return await this.pupilService.findByID(req.user.userId, req.user);
    }
    if(req.user.roles.includes(Role.Teacher)) {
      return await this.userService.findByID(req.user.userId);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('/user/:id')
  async getSelf(@Request() req, @Param() params): Promise<User> {
    let user: User;

    if(params.id === 'self') {
      user = await this.userService.findByID(req.user.userId);
    } else {
      user = await this.userService.findByID(params.id);
    }

    if(!user) {
      throw new NotFoundException();
    }

    return user;
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/user')
  update(@Request() req, @Body() updateUserDto: UpdateUserDto) {
    console.log(req.user);
    if((req.user.roles as string[]).includes(Role.Pupil)) {
      return this.pupilService.update(req.user.userId, updateUserDto, req.user);
    }
    return this.userService.update(req.user.userId, updateUserDto, req.user);
  }
}
