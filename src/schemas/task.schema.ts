import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

enum EFieldItemType {
  clear,
  water
}

interface IPuzzle {
  type: string;
  count: number;
}

interface IFieldItem {
  type: EFieldItemType;
}

@Schema()
export class Task {
  @Prop({required: true})
  levelID: string;

  @Prop({required: true, default: ''})
  text: string;

  @Prop({required: true, default: ''})
  name: string;

  @Prop({required: true, default: ''})
  hint: string;

  @Prop({required: true})
  field: IFieldItem[][];

  @Prop({required: true})
  startPos: string;

  @Prop({required: true})
  finalPos: string;

  @Prop({required: true})
  puzzles: IPuzzle[];
}

export type TaskDocument = Task & Document;
export const TaskSchema = SchemaFactory.createForClass(Task);
