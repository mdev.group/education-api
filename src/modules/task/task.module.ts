import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Task, TaskSchema } from 'src/schemas/task.schema';
import { CourseModule } from '../course/course.module';
import { LevelModule } from '../level/level.module';
import { PupilModule } from '../pupil/pupil.module';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';

@Module({
  controllers: [TaskController],
  providers: [TaskService],
  imports: [
    MongooseModule.forFeature([{ name: Task.name, schema: TaskSchema }]),
    forwardRef(() => LevelModule),
    forwardRef(() => PupilModule)
  ],
  exports: [
    MongooseModule.forFeature([{ name: Task.name, schema: TaskSchema }]),
    TaskService,
  ],
})
export class TaskModule {}
