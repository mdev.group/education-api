import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export enum EUserType {
  Teacher,
  Student
}

@Schema()
export class NotifyItem {
  @Prop()
  url?: string;

  @Prop({required: true})
  text: string;
}

@Schema()
export class Notification {
  @Prop({required: true})
  data: NotifyItem[];

  @Prop({required: true})
  create_datetime: number;

  @Prop({required: true})
  userType: EUserType;

  @Prop({required: true})
  userID: string;

  @Prop()
  createdBy?: string;
}

export type NotificationDocument = Notification & Document;
export const NotificationSchema = SchemaFactory.createForClass(Notification);
