import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, forwardRef, Inject, NotFoundException } from '@nestjs/common';
import { User, UserDocument } from 'src/schemas/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { genSaltSync, hashSync } from 'bcrypt';
import { PupilService } from '../pupil/pupil.service';
import { NotificationService } from '../notification/notification.service';
import { EUserType } from 'src/schemas/notification.schema';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private UserModel: Model<UserDocument>,
    @Inject(forwardRef(() => PupilService))
    private pupilService: PupilService,
    @Inject(forwardRef(() => NotificationService))
    private notificationService: NotificationService,
  ) {}

  async create(data: CreateUserDto): Promise<User> {
    try {
      if (!!await this.pupilService.findByUsername(data.email)) {
        throw new ForbiddenException('username already exists (Pupil)')
      }

      const salt = genSaltSync(10);
      const hashedPass = hashSync(data.password, salt);

      const created = new this.UserModel({
        email: data.email,
        password: hashedPass,
        reg_datetime: Number(new Date()),
        first_name: data.first_name,
        last_name: data.last_name,
      });

      if(created) {
        const res = await created.save();
        this.notificationService.create(res._id, EUserType.Teacher, [{text: `Добро пожаловать, ${data.first_name}!`}], null)

        return res;
      } else {
        throw new ForbiddenException();
      }
    } catch {
      throw new ForbiddenException();
    }
  }

  async findAll(): Promise<User[]> {
    return this.UserModel.find().exec();
  }

  async findByID(ID): Promise<User> {
    const teacher = await this.UserModel.findById(ID);

    if(!teacher) {
      throw new NotFoundException("User")
    }

    return teacher;
  }

  async findByEmail(email): Promise<User> {
    try {
      const filter: FilterQuery<UserDocument> = {
        email
      };

      return await this.UserModel.findOne(filter).lean().exec();
    } catch {
      return null;
    }
  }

  async update(ID, data, user): Promise<User> {
    this.notificationService.create(ID, EUserType.Teacher, [{text: "Отредактирована информация профиля"}], user)
    
    return this.UserModel
      .findByIdAndUpdate(ID, {
        $set: {
          ...removeEmptyFields(data),
        },
      },
      {new: true})
      .exec();
  }
}
