/* eslint-disable @typescript-eslint/no-var-requires */
const sgMail = require('@sendgrid/mail');
import type {MailService} from '@sendgrid/mail';


let init;

export default ():MailService => {
  if(init) {
    return sgMail;
  }
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);

  return sgMail;
};
