import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, forwardRef, Inject } from '@nestjs/common';
import { Pupil, PupilDocument } from 'src/schemas/pupil.schema';
import { InjectModel } from '@nestjs/mongoose';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { genSaltSync, hashSync } from 'bcrypt';
import { GroupService } from '../group/group.service';
import { letFields } from 'src/utils/letFields';
import { CourseService } from '../course/course.service';
import { UserService } from '../user/user.service';
import { NotificationService } from '../notification/notification.service';
import { EUserType } from 'src/schemas/notification.schema';
import { User } from 'src/schemas/user.schema';
import { Group } from 'src/schemas/group.schema';

@Injectable()
export class PupilService {
  constructor(
    @InjectModel(Pupil.name) private PupilModel: Model<PupilDocument>,
    @Inject(forwardRef(() => GroupService))
    private groupService: GroupService,
    @Inject(forwardRef(() => CourseService))
    private courseService: CourseService,
    @Inject(forwardRef(() => UserService))
    private userService: UserService,
    @Inject(forwardRef(() => NotificationService))
    private notificationService: NotificationService,
  ) {}

  userIsTeacher(pupil: Pupil, user) {
    return pupil.creatorID === user.userId;
  }

  async userIsStudentOfCourse(courseID: string, user) {
    const pupil = await this.findByID(user.userId, user);
    return pupil.courses.some((item) => {
      return item.ID == courseID;
    });
  }

  async userIsStudentOfGroup(groupID, user) {
    const pupil = await this.findByID(user.userId, user);
    return pupil.groupID == groupID;
  }

  userIsSelfPupil(pupil: any, user) {
    return pupil._id === user.userId;
  }

  async searchByUsername(search, user): Promise<Pupil[]> {
    return await this.PupilModel.aggregate([
    {
      $match: {
        username: {$regex: search, $options: 'i'},
        $or: [
          {
            creatorID: user.userId
          }, {
            creatorID: null
          }
        ]
      }
    },
    {
      $limit: 4
    }
    ])
    .exec()
  }

  async create(data, user): Promise<Pupil> {
    try {
      const salt = genSaltSync(10);
      const hashedPass = hashSync(data.password, salt);

      if (!!await this.userService.findByEmail(data.username)) {
        throw new ForbiddenException('username already exists (Teacher)')
      }

      const created = new this.PupilModel({
        username: data.username,
        password: hashedPass,
        first_name: data.first_name,
        last_name: data.last_name,
        reg_datetime: Number(new Date()),
        creatorID: user.userId || null
      });

      if(created) {
        return created.save();
      } else {
        throw new ForbiddenException();
      }
    } catch {
      throw new ForbiddenException();
    }
  }

  async findByCreatorID(creatorID): Promise<Pupil[]> {
    return this.PupilModel.find({
      creatorID
    }).exec();
  }

  async findByUsername(username): Promise<Pupil> {
    return this.PupilModel.findOne({
      username
    }).lean().exec();
  }

  async findByID(ID, user): Promise<Pupil> {
    const pupil = await this.PupilModel.findById(ID);

    if(!pupil) {
      throw new NotFoundException("Pupil")
    }

    return pupil;
  }

  async findByGroupID(groupID, user, checkGroup = true): Promise<Pupil[]> {
    if(checkGroup) {
      const group = await this.groupService.findByID(groupID, user);
      if(!group) {
        throw new NotFoundException('Group')
      }
      if(!this.groupService.userIsOwner(group, user) && !this.userIsStudentOfGroup(groupID, user)) {
        throw new NotAcceptableException('Group')
      }
    }

    const filter: FilterQuery<PupilDocument> = {
      groupID
    };

    return (await this.PupilModel.find(filter).lean().exec()) || [];
  }

  async findByCourseID(courseID, user): Promise<Pupil[]> {
    const filter: FilterQuery<PupilDocument> = {
      'courses.ID': courseID
    };

    // TODO: Проверить возможность чтения хотя бы первого Pupil'а

    return (await this.PupilModel.find(filter).lean().exec()) || [];
  }

  async update(ID, data, user): Promise<Pupil> {
    const pupil = await this.PupilModel.findById(ID);

    if(!pupil) {
      throw new NotFoundException("Pupil")
    }
    if(!this.userIsTeacher(pupil, user) && user.userId !== ID) {
      throw new NotAcceptableException("Pupil")
    }
    
    const acceptFields: (keyof Pupil)[] = ['photo_url', 'birth_date', 'first_name', 'last_name', 'photo_url'];

    const updFields = letFields(removeEmptyFields(data), acceptFields)

    await pupil.update({
      $set: updFields,
    }).exec()

    this.notificationService.create(pupil._id, EUserType.Student, [
      {
        text: "Отредактирована информация профиля"
      }
    ], user)

    return {
      ...pupil.toObject(),
      ...updFields
    }
  }

  async assignToGroup(ID, groupID, user): Promise<Pupil> {
    const pupil = await this.PupilModel.findById(ID);
    if(!pupil) {
      throw new NotFoundException('Pupil')
    }
    if(!this.userIsTeacher(pupil, user) && pupil.creatorID) {
      throw new NotAcceptableException('Pupil')
    }

    let group: Group;
    if(groupID !== null) {
      group = await this.groupService.findByID(groupID, user);
      if(!group) {
        throw new NotFoundException('Group')
      }
      if(!this.groupService.userIsOwner(group, user)) {
        throw new NotAcceptableException('Group')
      }

      await this.assignToCourse(pupil._id, group.courseID, true, user);

      this.notificationService.create(group.teacherID, EUserType.Teacher, [
        {
          text: `У Вас новый ученик в группе "${group.name}" - `
        },
        {
          text: pupil.first_name,
          url: `/student/${pupil._id}`
        }
      ], user)
  
    }

    await pupil.update({
      $set: {
        groupID
      }
    }).exec();

    return {
      ...pupil.toObject(),
      groupID
    }
  }

  async assignToTeacher(ID, teacherID, user): Promise<Pupil> {
    const pupil = await this.PupilModel.findById(ID);
    if(!pupil) {
      throw new NotFoundException('Pupil')
    }
    if(user.userId !== ID) {
      throw new NotAcceptableException('Pupil')
    }

    let teacher;
    if(teacherID !== null) {
      teacher = await this.userService.findByID(teacherID);
      if(!teacher) {
        throw new NotFoundException('Teacher')
      }
    }

    await pupil.update({
      $set: {
        creatorID: teacherID,
        courses: []
      }
    }).exec();

    this.notificationService.create(teacherID, EUserType.Teacher, [
      {
        text: "У Вас новый ученик - "
      },
      {
        text: pupil.first_name,
        url: `/student/${pupil._id}`
      }
    ], user)
    this.notificationService.create(ID, EUserType.Student, [
      {
        text: "У Вас новый учитель - "
      },
      {
        text: teacher.first_name,
        url: `/teacher/${teacher._id}`
      }
    ], user)

    return {
      ...pupil.toObject(),
      creatorID: teacherID
    }
  }

  async assignToCourse(ID, courseID, value, user): Promise<Pupil> {
    const pupil = await this.PupilModel.findById(ID);
    if(!pupil) {
      throw new NotFoundException('Pupil')
    }
    if(!this.userIsTeacher(pupil, user) && pupil.creatorID && user.userId !== pupil._id) {
      throw new NotAcceptableException('Pupil')
    }

    const course = await this.courseService.findByID(courseID, user);
    if(!course) {
      throw new NotFoundException('Course')
    }

    let newValue;

    if(value) {
      if(pupil.courses.some((item) => item.ID == courseID)) {
        newValue = pupil.courses
      } else {
        newValue = [...pupil.courses, {
          ID: courseID,
          taskID: null
        }]
      }
    } else {
      newValue = pupil.courses.filter((item) => item.ID != courseID)
    }

    await pupil.update({
      $set: {
        courses: newValue
      }
    }).exec();

    return {
      ...pupil.toObject(),
      courses: newValue
    }
  }

  async setCourseProgress(pupilID, courseID, taskID, user): Promise<Pupil> {
    const pupil = await this.PupilModel.findById(pupilID);
    if(!pupil) {
      throw new NotFoundException('Pupil')
    }
    if(!this.userIsStudentOfCourse(courseID, user)) {
      throw new NotAcceptableException('Pupil')
    }

    const newValue = pupil.courses.map((el) => {
      if(el.ID === courseID) {
        return {
          ...el,
          taskID
        }
      }
      return el;
    })

    await pupil.update({
      $set: {
        courses: newValue
      }
    }).exec();

    return {
      ...pupil.toObject(),
      courses: newValue
    }
  }

  async getAssignedCourses(ID, user): Promise<any> {
    if(ID !== user.userId) {
      throw new NotAcceptableException();
    }

    const pupil = await this.PupilModel.findById(ID);

    if(!pupil) {
      throw new NotFoundException("Pupil")
    }

    const courses = await Promise.all(pupil.courses.map(async (course) => {
      return await this.courseService.getStructureByID(course.ID, user);
    }))

    return courses;
  }
}
