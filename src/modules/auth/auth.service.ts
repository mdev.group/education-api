import { ForbiddenException, Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { compareSync } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { TelegramLoginPayload } from 'node-telegram-login';
import { verify } from 'jsonwebtoken';
import { Role } from './role.enum';
import { PupilService } from '../pupil/pupil.service';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private pupilService: PupilService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const pupilUser = await this.pupilService.findByUsername(email);
    const teacherUser = await this.userService.findByEmail(email);

    const user = teacherUser || pupilUser;

    if (user && compareSync(password, user.password)) {
      const { password, ...result } = user;
      return result;
    }

    throw new ForbiddenException("User not Found");
  }

  async login(user: any) {
    console.log(user);
    const roles: Role[] = user.username ? [
      Role.Pupil
    ] : [
      Role.Teacher
    ]

    const payload =
    {
      sub: user._id,
      iat: Math.floor(Number(new Date()) / 1000),
      tgID: user.tgID,
      roles: roles
    };
    return {
      access_token: this.jwtService.sign(payload, {
        expiresIn: '120s'
      }),
      refresh_token: this.jwtService.sign(payload, {
        expiresIn: '30d'
      }),
      expires_in: 120
    };
  }

  async register(data: any, user: any) {
    if(data.user_type == 'pupil') {
      return this.pupilService.create({
        username: data.email,
        password: data.password,
        first_name: data.first_name,
        last_name: data.last_name
      }, user)
    } else if (data.user_type == 'teacher') {
      return this.userService.create({
        email: data.email,
        password: data.password,
        first_name: data.first_name,
        last_name: data.last_name,  
        photo_url: data.photo_url
      })
    }
  }

  async refresh(oldToken: any) {
    try {
      const old_payload = verify(oldToken, process.env.JWT_SECRET) as any;

      const payload =
      {
        sub: old_payload.sub,
        iat: Math.floor(Number(new Date()) / 1000),
        tgID: old_payload.tgID,
        roles: old_payload.roles
      };
      return {
        access_token: this.jwtService.sign(payload, {
          expiresIn: '120s'
        }),
        refresh_token: this.jwtService.sign(payload, {
          expiresIn: '30d'
        }),
        expires_in: 120
      };
    } catch {
      return null
    }
  }
}
