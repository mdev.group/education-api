import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Task } from 'src/schemas/task.schema';
import { TaskService } from './task.service';
import { Role } from '../auth/role.enum';
import RoleGuard from '../auth/role.guard';
import { PupilService } from '../pupil/pupil.service';
import { Pupil } from 'src/schemas/pupil.schema';

@Controller()
export class TaskController {
  constructor(
    private readonly taskService: TaskService
  ) {}

  @UseGuards(RoleGuard([Role.Teacher]))
  @Get('/task/:id')
  get(@Request() req, @Param() params) {
    return this.taskService.findByID(params.id, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Get('/task/level/:id')
  getByCourseID(@Request() req, @Param() params) {
    return this.taskService.findByLevelID(params.id, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Post('/task')
  create(@Request() req, @Body() data) {
    return this.taskService.create(data, req.user);
  }

  @UseGuards(RoleGuard([Role.Pupil]))
  @Post('/task/:id/solve')
  solve(@Request() req, @Param() params) {
    return this.taskService.solve(params.id, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Patch('/task/:id')
  update(@Request() req, @Body() data, @Param() params) {
    return this.taskService.update(params.id, data, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Delete('/task/:id')
  async delete(@Request() req, @Param() params): Promise<any> {
    return await this.taskService.delete(params.id, req.user);
  }
}
