import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Notification } from 'src/schemas/notification.schema';
import { NotificationService } from './notification.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import RoleGuard from '../auth/role.guard';
import { Role } from '../auth/role.enum';

@Controller()
export class NotificationController {
  constructor(private readonly notificationService: NotificationService) {}

  @UseGuards(RoleGuard([Role.Teacher, Role.Pupil]))
  @Get('/notification')
  async find(@Request() req, @Body() data): Promise<Notification[]> {
    return await this.notificationService.findByUserID(req.user.userId, req.user);
  }

}
