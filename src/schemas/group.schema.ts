import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Group {
  @Prop({required: true})
  name: string;

  @Prop({required: true})
  teacherID: string;

  @Prop()
  courseID: string;
}

export type GroupDocument = Group & Document;
export const GroupSchema = SchemaFactory.createForClass(Group);
