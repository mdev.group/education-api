export function letFields(obj: any, accept: string[]) {
  return Object.fromEntries(
    Object.keys(obj)
      .filter((key) => accept.includes(key))
      .map((key) => {
        return [key, obj[key]];
      }),
  );
}
