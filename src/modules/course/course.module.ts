import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Course, CourseSchema } from 'src/schemas/course.schema';
import { LevelModule } from '../level/level.module';
import { PupilModule } from '../pupil/pupil.module';
import { UserModule } from '../user/user.module';
import { CourseController } from './course.controller';
import { CourseService } from './course.service';

@Module({
  controllers: [CourseController],
  providers: [CourseService],
  imports: [
    MongooseModule.forFeature([{ name: Course.name, schema: CourseSchema }]),
    forwardRef(() => PupilModule),
    forwardRef(() => LevelModule),
    forwardRef(() => UserModule)
  ],
  exports: [
    MongooseModule.forFeature([{ name: Course.name, schema: CourseSchema }]),
    CourseService,
  ],
})
export class CourseModule {}
