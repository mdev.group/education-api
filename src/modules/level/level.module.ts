import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Level, LevelSchema } from 'src/schemas/level.schema';
import { CourseModule } from '../course/course.module';
import { PupilModule } from '../pupil/pupil.module';
import { TaskModule } from '../task/task.module';
import { LevelController } from './level.controller';
import { LevelService } from './level.service';

@Module({
  controllers: [LevelController],
  providers: [LevelService],
  imports: [
    MongooseModule.forFeature([{ name: Level.name, schema: LevelSchema }]),
    forwardRef(() => CourseModule),
    forwardRef(() => TaskModule)
  ],
  exports: [
    MongooseModule.forFeature([{ name: Level.name, schema: LevelSchema }]),
    LevelService,
  ],
})
export class LevelModule {}
