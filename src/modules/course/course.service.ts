import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, forwardRef, Inject } from '@nestjs/common';
import { Course, CourseDocument } from 'src/schemas/course.schema';
import { InjectModel } from '@nestjs/mongoose';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { genSaltSync, hashSync } from 'bcrypt';
import { PupilService } from '../pupil/pupil.service';
import { LevelService } from '../level/level.service';
import { UserService } from '../user/user.service';

@Injectable()
export class CourseService {
  constructor(
    @InjectModel(Course.name) private CourseModel: Model<CourseDocument>,
    @Inject(forwardRef(() => PupilService))
    private pupilService: PupilService,
    @Inject(forwardRef(() => LevelService))
    private levelService: LevelService,
    @Inject(forwardRef(() => UserService))
    private userService: UserService,
  ) {}

  userIsOwner(course: Course, user) {
    return course.teacherID === user.userId;
  }

  async userIsStudent(courseID, user) {
    return await this.pupilService.userIsStudentOfCourse(courseID, user)
  }

  async create(data, user): Promise<Course> {
    const created = new this.CourseModel({
      ...data,
      create_datetime: Number(new Date()),
      teacherID: user.userId,
    });

    if(created) {
      return created.save();
    } else {
      throw new ForbiddenException();
    }
  }

  async appendEntries (course, user): Promise<CourseDocument> {
    const levels = await this.levelService.getAllByCourseIDStruct(course._id, user);
    const teacher = await this.userService.findByID(course.teacherID);

    return {
      ...course.toObject(),
      levels,
      teacher
    };
  }

  async findByID(ID, user): Promise<CourseDocument> {
    const course = await this.CourseModel.findById(ID);

    if(!course) {
      throw new NotFoundException("findByID - Course")
    }

    return course;
  }

  async getStructureByID(ID, user): Promise<any> {
    const course = await this.findByID(ID, user);
    
    return await this.appendEntries(course, user);
  }

  async findAll(user): Promise<Course[]> {
    const courses = await this.CourseModel.find().exec();

    const coursesAppended = await Promise.all(courses.map(async (course) => {
      return await this.appendEntries(course, user);
    }))

    return coursesAppended;
  }

  async findByCreatorID(teacherID, user): Promise<Course[]> {
    if(teacherID != user.userId) {
      throw new NotAcceptableException("Course")
    }

    const courses = await this.CourseModel.find({
      teacherID
    }).exec();

    const coursesAppended = await Promise.all(courses.map(async (course) => {;
      return await this.appendEntries(course, user);
    }))

    return coursesAppended;
  }

  async searchByName(search, user): Promise<Course[]> {
    return await this.CourseModel.aggregate([
    {
      $match: {
        name: {$regex: search, $options: 'i'},
        $or: [
          {teacherID: user.userId},
          {show: true}
        ]
      },
    },
    {
      $limit: 4
    }
    ])
    .exec()
  }

  async update(ID, data, user): Promise<Course> {
    const course = await this.CourseModel.findById(ID);
    if(!course) {
      throw new NotFoundException("Course")
    }
    if(!this.userIsOwner(course, user)) {
      throw new NotAcceptableException("Course")
    }

    return this.CourseModel
      .findByIdAndUpdate(ID, {
        $set: {
          ...removeEmptyFields(data),
        },
      },
      {new: true})
      .exec();
  }

  async delete(ID, user): Promise<any> {
    const course = await this.CourseModel.findById(ID);
    if(!course) {
      throw new NotFoundException("Course")
    }
    if(!this.userIsOwner(course, user)) {
      throw new NotAcceptableException("Course")
    }

    if((await this.pupilService.findByCourseID(ID, user)).length > 0) {
      throw new ForbiddenException("Pupils in course")
    }

    course.deleteOne()

    return {
      res: true
    };
  }
}
