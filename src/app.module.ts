import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { MongooseModule } from '@nestjs/mongoose';

import { AuthModule } from './modules/auth/auth.module';
import { CourseModule } from './modules/course/course.module';
import { GroupModule } from './modules/group/group.module';
import { LevelModule } from './modules/level/level.module';
import { PupilModule } from './modules/pupil/pupil.module';
import { TaskModule } from './modules/task/task.module';
import { UserModule } from './modules/user/user.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: async () => ({
        uri: process.env.DB_CONN,
      }),
    }),
    AuthModule,
    UserModule,
    PupilModule,
    GroupModule,
    CourseModule,
    LevelModule,
    TaskModule
  ]
})
export class AppModule {}
