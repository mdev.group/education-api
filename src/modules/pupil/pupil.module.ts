import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Pupil, PupilSchema } from 'src/schemas/pupil.schema';
import { CourseModule } from '../course/course.module';
import { GroupModule } from '../group/group.module';
import { NotificationModule } from '../notification/notification.module';
import { UserModule } from '../user/user.module';
import { PupilController } from './pupil.controller';
import { PupilService } from './pupil.service';

@Module({
  controllers: [PupilController],
  providers: [PupilService],
  imports: [
    MongooseModule.forFeature([{ name: Pupil.name, schema: PupilSchema }]),
    forwardRef(() => GroupModule),
    forwardRef(() => CourseModule),
    forwardRef(() => UserModule),
    forwardRef(() => NotificationModule)
  ],
  exports: [
    MongooseModule.forFeature([{ name: Pupil.name, schema: PupilSchema }]),
    PupilService,
  ],
})
export class PupilModule {}
