import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Group } from 'src/schemas/group.schema';
import { GroupService } from './group.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import RoleGuard from '../auth/role.guard';
import { Role } from '../auth/role.enum';

@Controller()
export class GroupController {
  constructor(private readonly groupService: GroupService) {}

  @UseGuards(RoleGuard([Role.Teacher]))
  @Post('/group')
  async create(@Request() req, @Body() data): Promise<Group> {
    return await this.groupService.create(data, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Get('/group/owned')
  async getOwned(@Request() req): Promise<Group[]> {
    return await this.groupService.findByTeacherID(req.user.userId, req.user);
  }

  @UseGuards(RoleGuard([Role.Teacher, Role.Pupil]))
  @Get('/group/:id')
  async getByID(@Param() params, @Request() req): Promise<Group> {
    return await this.groupService.findByID(params.id, req.user, true);
  }

  @UseGuards(RoleGuard([Role.Teacher]))
  @Delete('/group/:id')
  async delete(@Param() params, @Request() req): Promise<Group> {
    return await this.groupService.delete(params.id, req.user);
  }
}
