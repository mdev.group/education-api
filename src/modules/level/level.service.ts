import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, forwardRef, Inject } from '@nestjs/common';
import { Level, LevelDocument } from 'src/schemas/level.schema';
import { InjectModel } from '@nestjs/mongoose';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { genSaltSync, hashSync } from 'bcrypt';
import { PupilService } from '../pupil/pupil.service';
import { CourseService } from '../course/course.service';
import { CourseDocument } from 'src/schemas/course.schema';
import { TaskService } from '../task/task.service';

@Injectable()
export class LevelService {
  constructor(
    @InjectModel(Level.name) private LevelModel: Model<LevelDocument>,
    @Inject(forwardRef(() => CourseService))
    private courseService: CourseService,
    @Inject(forwardRef(() => TaskService))
    private taskService: TaskService,
  ) {}

  async getCourse(level: Level, user) {
    const course = await this.courseService.findByID(level.courseID, user);
    return course;
  }

  async userIsOwner(level: Level, user) {
    const course = await this.courseService.findByID(level.courseID, user);
    return course.teacherID === user.userId;
  }

  async userIsStudent(level: Level, user) {
    const course = await this.courseService.findByID(level.courseID, user);
    return await this.courseService.userIsStudent(course._id, user);
  }

  async create(data, user): Promise<Level> {
    const created = new this.LevelModel({
      order: data.order,
      name: data.name,
      courseID: data.courseID,
    });

    if(created) {
      return created.save();
    } else {
      throw new ForbiddenException();
    }
  }

  async findByID(ID, user): Promise<Level> {
    const level = await this.LevelModel.findById(ID);
    
    if(!level) {
      throw new NotFoundException("Level")
    }

    return level;
  }

  async getAllByCourseIDStruct(ID, user): Promise<any> {
    const levels = await this.findByCourseID(ID, user);
    
    const levelsWithTasks = await Promise.all(levels.map(async (level) => {
      const tasks = await this.taskService.findByLevelID(level._id, user);
      return {
        ...level.toObject(),
        tasks
      }
    }));

    return levelsWithTasks;
  }

  async findByCreatorID(teacherID, user): Promise<any> {
    const courses = await this.courseService.findByCreatorID(teacherID, user);

    const levels = await Promise.all(courses.map(async (item: CourseDocument) => this.findByCourseID(item._id, user)))
    const res = levels.reduce((prev, curr) => ([...prev, ...curr]), [])

    return res;
  }

  async findByCourseID(courseID, user): Promise<LevelDocument[]> {
    await this.courseService.findByID(courseID, user);

    return await this.LevelModel.find({
      courseID
    }).exec();
  }

  async update(ID, data, user): Promise<Level> {
    const level = await this.findByID(ID, user);

    return this.LevelModel
      .findByIdAndUpdate(ID, {
        $set: {
          ...removeEmptyFields(data),
        },
      },
      {new: true})
      .exec();
  }

  async delete(ID, user): Promise<any> {
    const level = await this.findByID(ID, user);

    if(!this.userIsOwner(level, user)) {
      throw new NotAcceptableException("Level");
    }

    await this.LevelModel.findOneAndDelete(ID);

    return {
      res: true
    };
  }
}
