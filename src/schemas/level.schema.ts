import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Level {
  @Prop({required: true})
  name: string;

  @Prop({required: true})
  order: number;

  @Prop({required: true})
  courseID: string;
}

export type LevelDocument = Level & Document;
export const LevelSchema = SchemaFactory.createForClass(Level);
