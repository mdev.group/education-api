import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

interface IAwardAssign {
  ID: string;
  assign_datetime: number;
}

interface ICourseAssign {
  ID: string;
  taskID: string;
}

@Schema()
export class Pupil {
  @Prop({ required: true, unique: true })
  username: string;

  @Prop({ required: true })
  password: string;

  @Prop({ required: true })
  first_name: string;

  @Prop({ required: true })
  last_name: string;

  @Prop({ required: true })
  reg_datetime: number;

  @Prop()
  birth_date: number;

  @Prop()
  photo_url: string;

  @Prop()
  groupID: string;

  @Prop()
  creatorID: string;

  @Prop()
  awards: IAwardAssign[];

  @Prop()
  courses: ICourseAssign[];
}

export type PupilDocument = Pupil & Document;
export const PupilSchema = SchemaFactory.createForClass(Pupil);
